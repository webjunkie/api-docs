---
title: API Reference

language_tabs:
  - shell: curl
  - php

toc_footers:
  - <a href='http://www.hipventory.com/'>Hipventory Home</a>

includes:
  - errors

search: true
---

# Authentication

> To authorize, use this code:

```shell
# With shell, you can just pass the correct header with each request
curl "{api_endpoint_here}"
  -H "Authorization: Token {your_token}"
```

```php
<?php
require_once('hipventory-php/autoload.php');

$hipventory = new Swagger\Client\Api\OrdersApi();
$hipventory->getApiClient()->getConfig()->addDefaultHeader("Authorization", "Token {your_token}");
?>
```

> Make sure to replace `{your_token}` with your API key.

Hipventory uses tokens to allow access to the API. They identify you and your
store.

Hipventory expects for the token to be included in all API requests to the
server in a header that looks like the following:

`Authorization: Token {your_token}`

## About testing

When using test tokens (as opposed to live tokens), deliveries that you create
will immediately trigger an email. Photos submitted will never appear in live
widgets and you will not be charged. You can switch widgets to test mode in
your store settings.

# Orders

Hipventory works with your store and contacts customers after they placed an
order with your store. That's why we need to know about it!

With each order, you tell us all about the products that were ordered. This
way we always have the latest details on them.

Important: We currently do not support product variants, meaning products
with e.g. different sizes or colors. What does that mean? If you have a product,
make sure you always submit the same unique product ID for that product, no
matter which sizes or color the user ordered.

## Create an order

> Definition

```shell
POST https://www.hipventory.com/api/orders/
```

```php
<?php
$hipventory->orderCreate({order_object});
?>
```

> Example

```shell
curl https://www.hipventory.com/api/orders/ -X POST \
-H "Content-Type: application/json" \
-H "Authorization: Token <your_token>" \
-d '{
    "customer_email": "test@example.com",
    "customer_name": "Dexter Donovan",
    "customer_id": "",
    "order_id": "123",
    "order_created": "2011-11-11 11:11:11",
    "language": "en",
    "platform": "curl",
    "products": [
        {
            "custom_id": "987",
            "image": "http://example.com/shop/nike.jpg",
            "translations": {
                "en": {
                    "name": "Nike Internationalist",
                    "url": "http://example.com/shop/nike/"
                }
            }
        }
    ]
}'
```

```php
<?php
$hip_product = new Swagger\Client\Model\Product(array(
    'custom_id' => $product_id,
    'image' => ""
));

$product_translation = new Swagger\Client\Model\ProductTranslation(array(
    'name' => "Name",
    'url' => $url
));
$product_translations_arr = array(
    "en" => $product_translation
);

$hip_product->setTranslations($product_translations_arr);

$hip_order = new Swagger\Client\Model\Order(array(
    'customer_email' => $order->getCustomerEmail(),
    'customer_name' => $order->getCustomerName(),
    'order_id' => $order->getId(),
    'order_created' => $order->getCreatedAtDate()->toString('yyyy-MM-dd HH:mm:ss'),
    'language' => "en",
    'platform' => "PHP",
    'products' => [$hip_product]
));

$resp = $hipventory->orderCreate($hip_order);
?>
```

### Arguments

Parameter | Description
--------- | -----------
customer_email<br>**required** | The customer's email address
customer_name<br>**required** | The customer's full name
customer_id | Unique customer ID, leave empty for e.g. guests
order_id<br>**required** | Order ID unique to the store
order_created<br>**required** | Order creation date
language<br>**required** | Language code.<br>This specifies which product language and which email template we pick.
platform<br>**required** | Platform
products<br>**required** | List of [product](#the-product-object) objects

# Deliveries

Deliveries tell us when items of an order are sent to the customer. We will
then contact the customer at the appropriate time after that.

## Create a delivery

> Definition

```shell
POST https://www.hipventory.com/api/orders/{order_id}/deliveries/
```

```php
<?php
$hipventory->deliveryCreate({order_id}, {shipment_id}, {product_ids_array}, {delivery_date});
?>
```

> Example

```shell
curl https://www.hipventory.com/api/orders/123/deliveries/ -X POST \
-H "Content-Type: application/json" \
-H "Authorization: Token <your_token>" \
-d '{
    "delivery_id": "567",
    "products": ["987"],
    "delivered": "2011-11-12 11:11:11"
}'
```

```php
<?php
$resp = $hipventory->deliveryCreate($order_id, $shipment_id, $products_id_array, $delivered_date);
?>
```

### Arguments

Parameter | Description
--------- | -----------
order_id<br>**required** | Order ID unique to the store
delivery_id<br>**required** | A unique ID for the delivery.
delivered<br>**required** | The date the shipment was started on your side.
products<br>**required** | List of product IDs.<br>**Note**: The products have to be "registered" by being included in an order before, otherwise they will be ignored.
send_mail | Whether to send email to customer or not. Default: `True`


# Products

## The product object

This represents products in your database.

Because many stores are multi-language, locale specific attributes live in
translation objects and not directly in the product object.

For the language code, please use either a 2-letter code ("en") or 5-letter code ("en_US"),
according to what fits your store best. Use it consistently in any case, or
you will break functionality for end users.

### Attributes

Parameter | Description
--------- | -----------
custom_id<br>**required** | A unique ID for that product.
image<br>**required** | The primary image for this product as a URL. We will fetch and cache this.
translations<br>**required** | A map of [product translation](#the-product-translation-object) objects with language codes as keys.

## The product translation object

### Attributes

Parameter | Description
--------- | -----------
name<br>**required** | Product name
url<br>**required** | Absolute URL to product in local store
